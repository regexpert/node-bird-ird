Bird-IRD
=======

NodeJS interface to the Bird Internet Routing Daemon

Requirements
------------

* NodeJS
* Bird running locally

Installation
------------

With NPM in your project directory:

	npm install bird-ird

Using GIT inside your `node_modules` directory:

	git clone https://gitlab.com/regexpert/node-bird-ird.git

Example
-------


Output
------


API
---

### Properties


### Methods

